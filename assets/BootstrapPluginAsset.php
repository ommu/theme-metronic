<?php
/**
 * BootstrapPluginAsset
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 25 February 2020, 08:17 WIB
 * @link https://bitbucket.org/ommu/theme-metronic
 * 
 */

namespace themes\metronic\assets;

class BootstrapPluginAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@npm/bootstrap/dist';

    public $js = [
        'js/bootstrap.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG ? true : false,
    ];
}