<?php
/**
 * PoppersPluginAsset
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 27 February 2020, 01:35 WIB
 * @link https://bitbucket.org/ommu/theme-metronic
 * 
 */

namespace themes\metronic\assets;

class PoppersPluginAsset extends \yii\web\AssetBundle
{
	public $sourcePath = '@npm/popper.js/dist';

	public $js = [
		'umd/popper.min.js',
	];

	public $publishOptions = [
		'forceCopy' => YII_DEBUG ? true : false,
	];
}