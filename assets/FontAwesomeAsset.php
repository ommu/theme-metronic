<?php
/**
 * FontAwesomeAsset
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 25 February 2020, 08:17 WIB
 * @link https://bitbucket.org/ommu/theme-metronic
 * 
 */

namespace themes\metronic\assets;

class FontAwesomeAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/font-awesome';
    
    public $css = [
        'css/all.min.css',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG ? true : false,
    ];
}