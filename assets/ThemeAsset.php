<?php
/**
 * ThemeAsset
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 25 February 2020, 08:17 WIB
 * @link https://bitbucket.org/ommu/theme-metronic
 * 
 */

namespace themes\metronic\assets;

class ThemeAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@themes/metronic';
    
    public $css = [
        'https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700',
        // 'css/bootstrap.css',
        'css/layout.css',
    ];

    public $depends = [
        'themes\metronic\assets\FontAwesomeAsset',
        'themes\metronic\assets\FlaticonAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG ? true : false,
        'except' => [
            'assets/',
            'controllers/',
            'layouts/',
            'modules/',
            'site/',
            'views/',
            'demos/',
        ],
    ];
}