<?php
/**
 * ThemePluginAsset
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 25 February 2020, 08:17 WIB
 * @link https://bitbucket.org/ommu/theme-metronic
 * 
 */

namespace themes\metronic\assets;

class ThemePluginAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@themes/metronic';

    public $js = [
        'js/global/components/base/util.js',
        'js/global/components/base/header.js',
        'js/global/components/base/offcanvas.js',
        'js/global/components/base/menu.js',
        'js/global/components/base/toggle.js',
        'js/global/components/base/scrolltop.js',
        'js/global/layout/layout.js',
        'js/custom.js',
    ];

    public $depends = [
        'themes\metronic\assets\ThemeAsset',
        'yii\web\JqueryAsset',
        'themes\metronic\assets\PoppersPluginAsset',
        'themes\metronic\assets\BootstrapPluginAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG ? true : false,
        'except' => [
            'assets/',
            'controllers/',
            'layouts/',
            'modules/',
            'site/',
            'views/',
            'demos/',
        ],
    ];
}