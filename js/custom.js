//count total json (obj)
function countProperties(obj) {
	var prop;
	var propCount = 0;

	for (prop in obj) {
		propCount++;
	}
	return propCount;
}

// submit modal function
function submitModal() {
	$('#defaultModal form').submit(function(event) {
		var url		 = $(this).attr('action');
		var options = {
			type: 'POST',
			data: $(this).serialize(),
			dataType: 'json',
			success: function(response, textStatus, jqXHR) {
				if (typeof(response.error) != 'undefined') {
					if(response.error == 0) {
						var $modalForm = $('form[action="'+url+'"]').parents('.modal-body');
						if($modalForm.length > 0)
							$modalForm.html(response.message);
					}
					return false;

				} else {
					if(response.redirect != null)
						location.href = response.redirect;
					else {
						if(countProperties(response) > 0) {
							$('form[action="'+url+'"] .form-group').removeClass('has-error');
							$('form[action="'+url+'"] .form-group .form-control').removeClass('is-invalid');
							$('form[action="'+url+'"] .form-group .invalid-feedback').html('');
							for(i in response) {
								$('form[action="'+url+'"] .field-' + i ).addClass('has-error');
								$('form[action="'+url+'"] .field-' + i + ' .form-control').addClass('is-invalid');
								$('form[action="'+url+'"] .field-' + i + ' .invalid-feedback').html(response[i][0]);
							}
						}
					}
				}
			},
			complete: function(jqXHR, textStatus) {
				var redirect = jqXHR.getResponseHeader('X-Redirect');
				if(redirect != null)
					location.href = redirect;
			}
		}
		$.ajax(url, options);
		event.preventDefault();
	});
}

$(document).ready(function () {
	/* dialog load */
	$(document).on('click', '.modal-btn:not("[data-target]")', function (event) {
		var link = $(this).attr('href');
		$('#defaultModal .modal-content').load(link, function () {
			$('#defaultModal').modal({
				show: true
			});
			submitModal();
		});
		event.preventDefault();
	});
});