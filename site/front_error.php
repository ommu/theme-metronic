<?php
/**
 * @var $this app\components\View
 * @var $this front3nd\app\controllers\SiteController
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 11 August 2020, 10:13 WIB
 * @link https://bitbucket.org/ommu/theme-metronic
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;

$textColor = $exception->statusCode === 404 ? "text-yellow" : "text-red";
$url = Yii::$app->request->absoluteUrl;
$message = $name.' '.nl2br(Html::encode($message));
?>

<div class="text-center pt-5 pb-5">

    <h1 class="<?php echo $textColor;?>"><?php echo $exception->statusCode; ?></h1>
    <h3><?php echo nl2br(Html::encode($exception->getName())) ?></h3>
    <p class="pt-3"><?php echo nl2br(Html::encode($message)); ?></p>

    <div class="mt-5">
        <?php echo Html::a(Yii::t('app', 'Report this?'), ['/report/site/add', 'url'=>$url, 'message'=>$message], ['class'=>'modal-btn']);?>
    </div>

</div>