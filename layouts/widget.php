<?php 
/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="kt-portlet">
    <?php if(isset($title)) {?>
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title"><?php echo $title;?></h3>
        </div>
    </div>
    <?php }?>
    <div class="kt-portlet__body <?php echo !$paddingBody ? 'kt-portlet__body--fit' : '';?>">
        <?php echo $content; ?>
    </div>
</div>