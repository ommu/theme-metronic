<?php 
/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php 
$context = $this->context;
if ($context->hasMethod('isVisitorBanned')) {
    if ($context->isVisitorBanned() === true) {
        throw new \yii\web\ForbiddenHttpException(Yii::t('app', 'You are not allowed to access this page.'));
    }
}

$this->beginContent('@themes/metronic/layouts/admin_default.php');

if($this->cards == false)
    echo $content;

else {?>
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <?php echo $content; ?>
        </div>
    </div>
<?php }?>

<?php $this->endContent(); ?>