<?php
/**
 * Footer
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 24 February 2020, 08:00 WIB
 * @link https://bitbucket.org/ommu/theme-metronic
 * 
 */

namespace themes\metronic\components;

use Yii;
use yii\helpers\Html;

class Footer extends \yii\base\Widget
{
    /**
     * {@inheritdoc}
     */
	public $withBackground = true;
    /**
     * {@inheritdoc}
     */
	public $siteName = 'Your Company';

    /**
     * {@inheritdoc}
     */
	public function init()
	{
		if($this->view->context->action instanceof \yii\web\ErrorAction)
			$this->withBackground = false;

		if(!Yii::$app->isDemoTheme()) {
			$copyright = unserialize(Yii::$app->setting->get(join('_', [Yii::$app->id, 'copyright'])));
			$this->siteName = Html::a($copyright['name'], $copyright['url'] ? $copyright['url'] : ['/site/index'], ['title'=>$copyright['name']]);
		}
	}

    /**
     * {@inheritdoc}
     */
	public function run() 
	{
		$isDemoTheme = Yii::$app->isDemoTheme() ? true : false;

		return $this->render('footer', [
			'isDemoTheme' => $isDemoTheme,
		]);
	}
}