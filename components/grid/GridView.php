<?php
/**
 * GridView
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 4 May 2019, 12:24 WIB
 * @link https://bitbucket.org/ommu/theme-metronic
 */

namespace themes\metronic\components\grid;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class GridView extends \yii\grid\GridView
{
    /**
     * {@inheritdoc}
     */
    public $dataColumnClass = 'themes\metronic\components\grid\DataColumn';
    /**
     * {@inheritdoc}
     */
    public $options = ['class' => 'grid-view kt-datatable kt-datatable--default'];
    /**
     * {@inheritdoc}
     */
    public $tableOptions = ['class' => 'kt-datatable__table'];
    /**
     * {@inheritdoc}
     */
    public $headerRowOptions = ['class' => 'kt-datatable__row'];
    /**
     * {@inheritdoc}
     */
    public $rowOptions = ['class' => 'kt-datatable__row'];
    /**
     * {@inheritdoc}
     */
    public $layout = "{items}\n{beginPager}\n{pager}\n{summary}\n{endPager}\n";
    /**
     * {@inheritdoc}
     */
    public $theadCssClass = 'kt-datatable__head';
    /**
     * {@inheritdoc}
     */
    public $tbodyCssClass = 'kt-datatable__body';
    /**
     * {@inheritdoc}
     */
    public $parts = [];

    /**
     * {@inheritdoc}
     */
    public $pager = [
        'class' => 'themes\metronic\components\widgets\LinkPager',
        'options' => [
            'class' => 'kt-datatable__pager-nav',
        ],
        'linkOptions' => [
            'class' => 'kt-datatable__pager-link',
        ],
        'firstPageCssClass' => 'kt-datatable__pager-link--first',
        'lastPageCssClass' => 'kt-datatable__pager-link--last',
        'prevPageCssClass' => 'kt-datatable__pager-link--prev',
        'nextPageCssClass' => 'kt-datatable__pager-link--next',
        'activePageCssClass' => 'kt-datatable__pager-link--active',
        'disabledPageCssClass' => 'kt-datatable__pager-link--disabled',
        'disabledListItemSubTagOptions' => [
            'tag' => 'a',
            'class' => 'kt-datatable__pager-link',
        ],
        'prevPageLabel' => '<i class="flaticon2-back"></i>',
        'nextPageLabel' => '<i class="flaticon2-next"></i>',
        'firstPageLabel' => '<i class="flaticon2-fast-back"></i>',
        'lastPageLabel' => '<i class="flaticon2-fast-next"></i>',
    ];

	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
        parent::init();

        // pagination basic & circle
        if ($this->view->pagination != 'default') {
            $this->pager = [
                'options' => [
                    'class' => 'kt-pagination__links',
                ],
                'firstPageCssClass' => 'kt-pagination__link--first',
                'lastPageCssClass' => 'kt-pagination__link--last',
                'prevPageCssClass' => 'kt-pagination__link--prev',
                'nextPageCssClass' => 'kt-pagination__link--next',
                'activePageCssClass' => 'kt-pagination__link--active',
                'disabledPageCssClass' => 'kt-datatable__pager-link--disabled',
                'disabledListItemSubTagOptions' => [
                    'tag' => 'a',
                ],
                'prevPageLabel' => '<i class="fa fa-angle-left kt-font-brand"></i>',
                'nextPageLabel' => '<i class="fa fa-angle-right kt-font-brand"></i>',
                'firstPageLabel' => '<i class="fa fa-angle-double-left kt-font-brand"></i>',
                'lastPageLabel' => '<i class="fa fa-angle-double-right kt-font-brand"></i>',
            ];
        }

        if (!isset($this->parts['{beginPager}'])) {
            $options = [];
    
            if ($this->view->pagination == 'default')
                Html::addCssClass($options, 'kt-datatable__pager kt-datatable--paging-loaded');
            if ($this->view->pagination == 'basic')
                Html::addCssClass($options, 'kt-datatable__pager kt-pagination kt-pagination--brand');
            if ($this->view->pagination == 'circle')
                Html::addCssClass($options, 'kt-datatable__pager kt-pagination kt-pagination--brand kt-pagination--circle');
            $this->parts['{beginPager}'] = Html::beginTag('div', $options);
        }

        if (!isset($this->parts['{endPager}'])) {
            $this->parts['{endPager}'] = Html::endTag('div');
        }

        $this->layout = strtr($this->layout, $this->parts);
    }

    /**
     * {@inheritdoc}
     */
    public function renderTableHeader()
    {
        if (isset($this->theadCssClass)) {
            $parent = parent::renderTableHeader();
            return strtr($parent, ['<thead>' => '<thead class="'.$this->theadCssClass.'">']);
        }

        return parent::renderTableHeader();
    }

    /**
     * {@inheritdoc}
     */
    public function renderTableBody()
    {
        if (isset($this->tbodyCssClass)) {
            $parent = parent::renderTableBody();
            return strtr($parent, ['<tbody>' => '<tbody class="'.$this->tbodyCssClass.'">']);
        }

        return parent::renderTableBody();
    }

    /**
    * {@inheritdoc}
    */
    public function renderEmpty()
    {
        if ($this->emptyText === false) {
            return '';
        }
        $options = $this->emptyTextOptions;
        $options = ArrayHelper::merge($options, ['type'=>'secondary', 'class'=>'mt-3 mr-3 mb-0 ml-3']);

        return \app\components\widgets\Alert::widget(['body'=>$this->emptyText, 'options'=>$options, 'closeButton'=>false]);
    }
}
