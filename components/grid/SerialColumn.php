<?php
/**
 * SerialColumn
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 18 Fabruary 2020, 20:52 WIB
 * @link https://bitbucket.org/ommu/theme-metronic
 */

namespace themes\metronic\components\grid;

use Yii;
use yii\helpers\Html;

class SerialColumn extends \yii\grid\SerialColumn
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // headerOptions
        if (isset($this->headerOptions['class']))
            $this->headerOptions['class'] = join(' ', [$this->headerOptions['class'], 'kt-datatable__cell']);
        else
            Html::addCssClass($this->headerOptions, 'kt-datatable__cell');

        // contentOptions
        if (isset($this->contentOptions['class']))
            $this->contentOptions['class'] = join(' ', [$this->contentOptions['class'], 'kt-datatable__cell']);
        else
            Html::addCssClass($this->contentOptions, 'kt-datatable__cell');
    }
}
