<?php
/**
 * DataColumn
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 12 February 2020, 14:45 WIB
 * @link https://bitbucket.org/ommu/theme-metronic
 */

namespace themes\metronic\components\grid;

use Yii;
use yii\helpers\Html;

class DataColumn extends \yii\grid\DataColumn
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        // headerOptions
        if(isset($this->headerOptions['class']))
            $this->headerOptions['class'] = join(' ', [$this->headerOptions['class'], 'kt-datatable__cell']);
        else
            Html::addCssClass($this->headerOptions, 'kt-datatable__cell');

        // contentOptions
        if(isset($this->contentOptions['class']))
            $this->contentOptions['class'] = join(' ', [$this->contentOptions['class'], 'kt-datatable__cell']);
        else
            Html::addCssClass($this->contentOptions, 'kt-datatable__cell');

        // Sorter class
        $provider = $this->grid->dataProvider;
        if ($this->attribute !== null && $this->enableSorting && ($sort = $provider->getSort()) !== false && $sort->hasAttribute($this->attribute))
            $this->headerOptions['class'] = join(' ', [$this->headerOptions['class'], 'kt-datatable__cell--sort']);

        return parent::init();
    }
}
