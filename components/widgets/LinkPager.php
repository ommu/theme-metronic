<?php
/**
 * LinkPager
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 13 February 2020, 12:05 WIB
 * @link https://bitbucket.org/ommu/theme-metronic
 */

namespace themes\metronic\components\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class LinkPager extends \yii\widgets\LinkPager
{
    /**
     * {@inheritdoc}
     */
    protected function renderPageButton($label, $page, $class, $disabled, $active)
    {
        $options = $this->linkContainerOptions;
        $linkWrapTag = ArrayHelper::remove($options, 'tag', 'li');
        if(isset($this->pageCssClass))
            Html::addCssClass($options, $this->pageCssClass);

        $linkOptions = $this->linkOptions;
        $linkOptions['data-page'] = $page;

        if($class) {
            if(isset($linkOptions['class']))
                $linkOptions['class'] = join(' ', [$linkOptions['class'], $class]);
        }

        if ($active) {
            if(isset($linkOptions['class']))
                $linkOptions['class'] = join(' ', [$linkOptions['class'], $this->activePageCssClass]);
        }
        if ($disabled) {
            $disabledItemOptions = $this->disabledListItemSubTagOptions;
            $tag = ArrayHelper::remove($disabledItemOptions, 'tag', 'span');
            if($class) {
                if(isset($disabledItemOptions['class']))
                    $disabledItemOptions['class'] = join(' ', [$disabledItemOptions['class'], $class]);
            }
            if(isset($disabledItemOptions['class']))
                $disabledItemOptions['class'] = join(' ', [$disabledItemOptions['class'], $this->disabledPageCssClass]);

            return Html::tag($linkWrapTag, Html::tag($tag, $label, $disabledItemOptions), $options);
        }

        return Html::tag($linkWrapTag, Html::a($label, $this->pagination->createUrl($page), $linkOptions), $options);
    }
}
