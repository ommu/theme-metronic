<?php
/**
 * ListView
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 26 February 2020, 11:18 WIB
 * @link https://bitbucket.org/ommu/theme-metronic
 */

namespace themes\metronic\components\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class ListView extends \yii\widgets\ListView
{
    /**
     * {@inheritdoc}
     */
    public $layout = "{items}\n{beginPager}\n{pager}\n{summary}\n{endPager}\n";
    /**
     * {@inheritdoc}
     */
    public $parts = [];

    /**
     * {@inheritdoc}
     */
    public $pager = [
        'class' => 'themes\metronic\components\widgets\LinkPager',
        'options' => [
            'class' => 'kt-datatable__pager-nav',
        ],
        'linkOptions' => [
            'class' => 'kt-datatable__pager-link',
        ],
        'firstPageCssClass' => 'kt-datatable__pager-link--first',
        'lastPageCssClass' => 'kt-datatable__pager-link--last',
        'prevPageCssClass' => 'kt-datatable__pager-link--prev',
        'nextPageCssClass' => 'kt-datatable__pager-link--next',
        'activePageCssClass' => 'kt-datatable__pager-link--active',
        'disabledPageCssClass' => 'kt-datatable__pager-link--disabled',
        'disabledListItemSubTagOptions' => [
            'tag' => 'a',
            'class' => 'kt-datatable__pager-link',
        ],
        'prevPageLabel' => '<i class="flaticon2-back"></i>',
        'nextPageLabel' => '<i class="flaticon2-next"></i>',
        'firstPageLabel' => '<i class="flaticon2-fast-back"></i>',
        'lastPageLabel' => '<i class="flaticon2-fast-next"></i>',
    ];

	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
        parent::init();

        // pagination basic & circle
        if ($this->view->pagination != 'default') {
            $this->pager = [
                'options' => [
                    'class' => 'kt-pagination__links',
                ],
                'firstPageCssClass' => 'kt-pagination__link--first',
                'lastPageCssClass' => 'kt-pagination__link--last',
                'prevPageCssClass' => 'kt-pagination__link--prev',
                'nextPageCssClass' => 'kt-pagination__link--next',
                'activePageCssClass' => 'kt-pagination__link--active',
                'disabledPageCssClass' => 'kt-datatable__pager-link--disabled',
                'disabledListItemSubTagOptions' => [
                    'tag' => 'a',
                ],
                'prevPageLabel' => '<i class="fa fa-angle-left kt-font-brand"></i>',
                'nextPageLabel' => '<i class="fa fa-angle-right kt-font-brand"></i>',
                'firstPageLabel' => '<i class="fa fa-angle-double-left kt-font-brand"></i>',
                'lastPageLabel' => '<i class="fa fa-angle-double-right kt-font-brand"></i>',
            ];
        }

        if (!isset($this->parts['{beginPager}'])) {
            $options = [];
    
            if ($this->view->pagination == 'default')
                Html::addCssClass($options, 'kt-datatable__pager kt-datatable--paging-loaded mb-4');
            if ($this->view->pagination == 'basic')
                Html::addCssClass($options, 'kt-datatable__pager kt-pagination kt-pagination--brand mb-4');
            if ($this->view->pagination == 'circle')
                Html::addCssClass($options, 'kt-datatable__pager kt-pagination kt-pagination--brand kt-pagination--circle mb-4');
            $this->parts['{beginPager}'] = Html::beginTag('div', $options);
        }
        if (!isset($this->parts['{endPager}']))
            $this->parts['{endPager}'] = Html::endTag('div');
        $this->layout = strtr($this->layout, $this->parts);
    }
}
