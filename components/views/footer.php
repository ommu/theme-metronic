<?php
/**
 * @var string $content
 * @var $this app\components\View
 */

use yii\helpers\Html;
use yii\helpers\Url;

$themeAsset = \themes\metronic\assets\ThemeAsset::register($this);
$context = $this->context;
?>

<?php //begin.Footer ?>
<div class="kt-footer kt-footer--extended kt-grid__item" id="kt_footer" <?php echo $context->withBackground ? 'style="background-image: url('.$themeAsset->baseUrl.'/images/bg/bg-2.jpg)"' : '';?>>
    <?php /*
    <div class="kt-footer__top">
        <div class="kt-container ">
            <div class="row">
                <div class="col-lg-4">
                    <div class="kt-footer__section">
                        <h3 class="kt-footer__title">About</h3>
                        <div class="kt-footer__content">
                            Lorem Ipsum is simply dummy text of the printing
                            <br> and typesetting and typesetting industry has been the
                            <br> industry's standard dummy text ever since the 1500s,
                            <br> when an unknown printer took a galley of type.
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="kt-footer__section">
                        <h3 class="kt-footer__title">Quick Links</h3>
                        <div class="kt-footer__content">
                            <div class="kt-footer__nav">
                                <div class="kt-footer__nav-section">
                                    <a href="#">General Reports</a>
                                    <a href="#">Dashboart Widgets</a>
                                    <a href="#">Custom Pages</a>
                                </div>
                                <div class="kt-footer__nav-section">
                                    <a href="#">User Setting</a>
                                    <a href="#">Custom Pages</a>
                                    <a href="#">Intranet Settings</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="kt-footer__section">
                        <h3 class="kt-footer__title">Get In Touch</h3>
                        <div class="kt-footer__content">
                            <form action="" class="kt-footer__subscribe">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Enter Your Email">
                                    <div class="input-group-append">
                                        <button class="btn btn-brand" type="button">Join</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    */ ?>
    <div class="kt-footer__bottom">
        <div class="kt-container ">
            <div class="kt-footer__wrapper">
                <div class="kt-footer__logo">
                    <a class="kt-header__brand-logo" href="?page=index&amp;demo=demo2">
                        <img alt="Logo" src="<?php echo $themeAsset->baseUrl;?>/images/logos/logo-4-sm.png" class="kt-header__brand-logo-sticky">
                    </a>
                    <div class="kt-footer__copyright">
                        &copy; <?php echo date("Y");?> <?php echo $context->siteName;?>. All Rights Reserved.
                        <?php if($isDemoTheme) {?>Designed By <span class="text-white"> Keenthemes.</span><?php }?>
                    </div>
                </div>
                <?php /*
                <div class="kt-footer__menu">
                    <a href="http://keenthemes.com/metronic" target="_blank">Purchase Lisence</a>
                    <a href="http://keenthemes.com/metronic" target="_blank">Team</a>
                    <a href="http://keenthemes.com/metronic" target="_blank">Contact</a>
                </div>
                */ ?>
            </div>
        </div>
    </div>
</div>
<?php //end.Footer ?>